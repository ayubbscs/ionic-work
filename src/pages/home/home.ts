import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AboutUsPage } from '../about-us/about-us'
import { ContactusPage } from '../contactus/contactus'
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController , public alertCtrl: AlertController) {
  }

  public buttonClicked()
  {
      this.navCtrl.push(AboutUsPage);
  }

  public buttonClicked2()
  {
      this.navCtrl.push(ContactusPage);
  }

  loginAlert() {
    let alert = this.alertCtrl.create({
      title: 'Hi User',
      subTitle: 'Your Username & Password is correct !',
      buttons: ['OK']
    });
    alert.present();
  }

}
